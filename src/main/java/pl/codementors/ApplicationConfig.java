package pl.codementors;

import pl.codementors.resources.CVResource;
import pl.codementors.resources.CVResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * REST context configuration.
 *
 * @author psysiu
 */
@ApplicationPath("/api")//Define root path for REST services.
public class ApplicationConfig extends Application {

}
