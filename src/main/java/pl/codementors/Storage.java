package pl.codementors;

import pl.codementors.resources.Dosw;
import pl.codementors.resources.Edu;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Storage {

    private Cv cv;

    @PostConstruct
    public void init() {

        List<Dosw> doswiadczenie = new ArrayList<>();
        doswiadczenie.add(new Dosw("2008-2010","dotMedia","drukarz"));
        List<Edu> edukacja = new ArrayList<>();
        edukacja.add(new Edu("1998-2006","Sp5 Kraśnik","podstawowe"));
        List<String> zainteresowania = new ArrayList<>();
        zainteresowania.add("sport");
        zainteresowania.add("komputery");
        zainteresowania.add("muzyka");
        List<String> umiejetnosci = new ArrayList<>();
        umiejetnosci.add("szybko biegam na 10m");
        umiejetnosci.add("skacze nisko");
        umiejetnosci.add("turlam się koślawo");
        cv = new Cv( "sebastian", "żurawski","701 702 703",doswiadczenie, edukacja, umiejetnosci, zainteresowania);
    }

    public Cv getCv() {
        return cv;
    }
}
