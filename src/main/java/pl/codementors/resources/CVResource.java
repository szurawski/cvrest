package pl.codementors.resources;

import pl.codementors.Storage;
import pl.codementors.Cv;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static com.sun.javafx.fxml.expression.Expression.add;

/**
 * REST resource for managing books.
 */
@Path("cv")
public class CVResource {



    @Inject
    private Storage storage;

    @GET
    @Path("test")
    @Produces(MediaType.TEXT_PLAIN)
    public String test() {
        return "no cv here";
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Cv getCv() {
        return storage.getCv();
    }

    @POST
    @Path("doswiadczenie")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addDoswiadczenie(Dosw doswiadczenie) {
        storage.getCv().getDoswiadczenie().add(doswiadczenie);
    }


}
